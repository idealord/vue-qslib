
import GUID from './GUID';
import {QuestionDataTypes} from './QuestionDataTypes';
/**
 * 基本选项类定义
 */
export default class  BaseOption {
    public itemName: string = '';
    public itemId: string = '';
     // 是否可以填空
    public canFillBlank: boolean = false;
    public blankText: string = '';
    public memo: string = '';
    // 默认选中
    public defaultSel: boolean = false;
    // 选项最大值
    public MaxValue: number = 0;
    // 选项最小值
    public MinValue: number = 0;
    // 图片地址
    public ImageUrl: string = ' ';
    public blank="";
    public check="";
    public itemType: QuestionDataTypes = QuestionDataTypes.dtText;

    constructor() {
        this.itemId = new GUID().toString();
    }

}
