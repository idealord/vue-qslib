export default class IndexObj {
    public Index: number = 0;
    public Value: string = '';

    constructor(index: number, value: string) {
        this.Index = index;
        this.Value = value;
    }
}
